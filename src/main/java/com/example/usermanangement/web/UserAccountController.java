package com.example.usermanangement.web;

import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.payloads.request.UserAccountRequest;
import com.example.usermanangement.data.payloads.request.UserRequest;
import com.example.usermanangement.service.UserAccountService;
import com.example.usermanangement.data.payloads.response.MessageResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/user_account")
public class UserAccountController {
    @Autowired
    UserAccountService userAccountService;

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addUserAccount( @RequestBody UserAccountRequest user) {
        MessageResponse newUserAccount = userAccountService.createUserAccount(user);
        return new ResponseEntity<>(newUserAccount, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserAccount>> getAllUserAccount () {
        List<UserAccount> user = userAccountService.getAllUserAccount();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


    @GetMapping("/allInfo")
    public ResponseEntity<List<UserAccount>> getAllUser () {
        List<UserAccount> user = userAccountService.getUsers();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
