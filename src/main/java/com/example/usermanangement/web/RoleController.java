package com.example.usermanangement.web;

import com.example.usermanangement.data.models.Role;
import com.example.usermanangement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleService roleService;



    @GetMapping("/all")
    public ResponseEntity<List<Role>> getAllRole () {
        List<Role> role = roleService.getAllRole();
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Role> getRoleById (@PathVariable("id") Integer id) {
        Role role = roleService.getRoleById(id);
        return new ResponseEntity<>(role, HttpStatus.OK);
    }
}
