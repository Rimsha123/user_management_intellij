package com.example.usermanangement.web;

import com.example.usermanangement.data.DTO.UserDTO;
import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.payloads.request.UserRequest;
import com.example.usermanangement.data.repository.DTOProjection;
import com.example.usermanangement.data.repository.UserRepository;
import com.example.usermanangement.service.UserService;
import com.example.usermanangement.data.payloads.response.MessageResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;


    @GetMapping("/all")
    public ResponseEntity<List<User>> getAllUser () {
        List<User> user = userService.getAllUser();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<User> getUserById (@PathVariable("id") Integer id) throws Exception {
        User user = userService.getUserById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/addbyAdmin")
    public ResponseEntity<MessageResponse> addUser( @RequestBody UserRequest user) {
        MessageResponse newUser = userService.createUser(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PostMapping("/addbySignUp")
    public ResponseEntity<MessageResponse> addUserBySignUp( @RequestBody UserRequest user) {
        MessageResponse newUser = userService.createUserBySignUp(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<MessageResponse> updateUser( @PathVariable Integer id, @RequestBody UserRequest user){
        User updateU = userService.updateUser(id, user);
        return new ResponseEntity(updateU, HttpStatus.OK);
    }


    @PutMapping("/editUser/{id}")
    public ResponseEntity<MessageResponse> editUser( @PathVariable Integer id, @RequestBody UserRequest user){
        Optional<User> editUser = userService.editUser(id, user);
        return new ResponseEntity(editUser, HttpStatus.OK);
    }

    @PutMapping("/delete/{id}")
    public ResponseEntity<MessageResponse> deleteUser(@PathVariable("id") Integer id) {
        MessageResponse deletedUser = userService.deleteUser(id);
        return new ResponseEntity<>(deletedUser, HttpStatus.OK);
    }

    @GetMapping("/activeCount")
    public ResponseEntity<User> getActiveUser () throws Exception {
        Integer count = userService.getActiveCount();
        return new ResponseEntity(count, HttpStatus.OK);
    }

    @GetMapping("/totalCount")
    public ResponseEntity<User> getTotalUser () throws Exception {
        Integer totalCount = userService.getTotalCount();
        return new ResponseEntity(totalCount, HttpStatus.OK);
    }

    @GetMapping("/pendingCount")
    public ResponseEntity<User> getPendingUser () throws Exception {
        Integer pendingCount = userService.getPendingCount();
        return new ResponseEntity(pendingCount, HttpStatus.OK);
    }

    @GetMapping("/deletedCount")
    public ResponseEntity<User> getDeletedUser () throws Exception {
        Integer deletedCount = userService.getDeletedCount();
        return new ResponseEntity(deletedCount, HttpStatus.OK);
    }

    @GetMapping("/allUsers")
    public List<DTOProjection> getAllPosts() {
        List<DTOProjection> list = userRepository.GetUsers();
        List<UserDTO> dtos = new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        list.forEach(student -> dtos.add(mapper.map(list, UserDTO.class)));
        return list;
    }

    @GetMapping("/loginUser/{email}")
    public DTOProjection getLoginUser(@PathVariable("email") String email) throws Exception{
        DTOProjection loginUser = userService.getLoginUser(email);
        return loginUser;
//        return new ResponseEntity(loginUser, HttpStatus.OK);

    }

}
