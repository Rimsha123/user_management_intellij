package com.example.usermanangement.service;

import com.example.usermanangement.data.models.Role;
import com.example.usermanangement.data.repository.RoleRepository;

import com.example.usermanangement.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role getRoleById(Integer roleId){
        return roleRepository.findById(roleId).orElseThrow(()-> new ResourceNotFoundException("Resource Not Found"));
    }

    @Override
    public List<Role> getAllRole() {
        return roleRepository.findAll();
    }
}
