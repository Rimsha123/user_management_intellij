package com.example.usermanangement.service;

import com.example.usermanangement.data.models.Role;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public interface RoleService {
    Role getRoleById(Integer RoleId);
    List<Role> getAllRole();
}
