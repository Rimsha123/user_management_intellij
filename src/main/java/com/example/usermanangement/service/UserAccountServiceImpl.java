package com.example.usermanangement.service;
import com.example.usermanangement.data.payloads.request.UserAccountRequest;
import com.example.usermanangement.data.payloads.response.MessageResponse;
import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.repository.UserAccountRepository;
import com.example.usermanangement.data.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    public MessageResponse createUserAccount(UserAccountRequest userAccountRequest){
        UserAccount newUserAccount = new UserAccount();
        newUserAccount.setUser(userAccountRequest.getUser());
        newUserAccount.setRole(userAccountRequest.getRole());
        newUserAccount.setStatus(userAccountRequest.getStatus());

        userAccountRepository.save(newUserAccount);
        return new MessageResponse("New User Account created Successfully");
    }

    @Override
    public Optional<UserAccount> updateUserAccount(Integer userAccountId, UserAccountRequest userAccountRequest) {
        Optional<UserAccount> userAccount = userAccountRepository.findById(userAccountId);
        if(userAccount.isPresent()){
            userAccount.get().setUser(userAccountRequest.getUser());
            userAccount.get().setRole(userAccountRequest.getRole());
            userAccount.get().setStatus(userAccountRequest.getStatus());
            userAccountRepository.save(userAccount.get());
            return userAccount;
        }
        return userAccount;
    }

    @Override
    public void deleteUserAccount(Integer userAccountId) {
        if(userAccountRepository.getById(userAccountId).getId().equals(userAccountId)){
            userAccountRepository.deleteById(userAccountId);
        }
    }

    @Override
    public Optional<UserAccount> getUserAccountById(Integer userAccountId) {
        return userAccountRepository.findById(userAccountId);
    }

    @Override
    public List<UserAccount> getAllUserAccount() {
        return userAccountRepository.findAll();
    }

    @Override
    public List<UserAccount> getUsers(){
        return userAccountRepository.GetUsers();
    }


}
