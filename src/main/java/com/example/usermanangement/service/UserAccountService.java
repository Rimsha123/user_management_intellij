package com.example.usermanangement.service;
import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.payloads.response.MessageResponse;
import com.example.usermanangement.data.payloads.request.UserAccountRequest;
import com.example.usermanangement.data.models.User;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public interface UserAccountService {
    MessageResponse createUserAccount(UserAccountRequest userAccountRequest);
    Optional<UserAccount> updateUserAccount(Integer userAccountId, UserAccountRequest userAccountRequest);
    void deleteUserAccount(Integer userAccountId);
    Optional<UserAccount> getUserAccountById(Integer userAccountId);
    List<UserAccount> getAllUserAccount();
    List<UserAccount> getUsers();

}
