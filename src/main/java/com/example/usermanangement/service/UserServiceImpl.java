package com.example.usermanangement.service;
import com.example.usermanangement.data.DTO.UserDTO;
import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.payloads.request.UserRequest;
import com.example.usermanangement.data.payloads.response.MessageResponse;
import com.example.usermanangement.data.repository.DTOProjection;
import com.example.usermanangement.data.repository.UserAccountRepository;
import com.example.usermanangement.data.repository.UserRepository;
import com.example.usermanangement.exception.ResourceNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


   @Autowired
    private UserRepository userRepository;


   @Autowired
   private UserAccountRepository userAccountRepository;

   Logger logger;

    @Override
    public MessageResponse createUser(UserRequest userRequest){
        User newUser = new User();

        newUser.setFirstName(userRequest.getFirstName());
        newUser.setLastName(userRequest.getLastName());
        newUser.setUserName(userRequest.getFirstName());
        newUser.setEmail(userRequest.getEmail());
        newUser.setPhoneNumber(userRequest.getPhoneNumber());
        newUser.setPassword(userRequest.getPassword());
        newUser.setGender(userRequest.getGender());
        newUser.setDesignation(userRequest.getDesignation());

        newUser = userRepository.save(newUser);
        userRepository.flush();
        System.out.println(newUser.getId());
        UserAccount newUserAccount = new UserAccount();

        newUserAccount.setStatus("Active");

        newUserAccount.setUser(newUser);
        newUserAccount.setRole(userRequest.getRole());
        userAccountRepository.save(newUserAccount);
        return new MessageResponse("New User Created Successfully");
    }

    @Override
    public MessageResponse createUserBySignUp(UserRequest userRequest){
        User newUser = new User();

        newUser.setFirstName(userRequest.getFirstName());
        newUser.setLastName(userRequest.getLastName());
        newUser.setUserName(userRequest.getFirstName());
        newUser.setEmail(userRequest.getEmail());
        newUser.setPhoneNumber(userRequest.getPhoneNumber());
        newUser.setPassword(userRequest.getPassword());
        newUser.setGender(userRequest.getGender());
        newUser.setDesignation(userRequest.getDesignation());

        newUser = userRepository.save(newUser);
        userRepository.flush();
        System.out.println(newUser.getId());
        UserAccount newUserAccount = new UserAccount();

        newUserAccount.setStatus("Pending");

        newUserAccount.setUser(newUser);
        newUserAccount.setRole(userRequest.getRole());
        userAccountRepository.save(newUserAccount);
        return new MessageResponse("New User Created Successfully");
    }

    @Override
    public User updateUser(Integer userId, UserRequest userRequest ) throws ResourceNotFoundException{
        User user = userRepository.findById(userId).orElseThrow(()-> new ResourceNotFoundException("User Not Found"));
        Integer userAccountId = userAccountRepository.FindByUserId(user.getId());
        Optional<UserAccount> userAccount = userAccountRepository.findById(userAccountId);

        if ( !userAccount.isPresent()){
            throw new ResourceNotFoundException("User Not Found. cant updated");
        }
        else{

            user.setFirstName(userRequest.getFirstName());
            user.setLastName(userRequest.getLastName());
            user.setUserName(userRequest.getUserName());
            user.setPhoneNumber(userRequest.getPhoneNumber());
            user.setEmail(userRequest.getEmail());
            user.setPassword(userRequest.getPassword());
            user.setDesignation(userRequest.getDesignation());
            user.setGender(userRequest.getGender());
            userRepository.save(user);
            userAccount.get().setStatus("Pending");

            userAccount.get().setUser(user);
            userAccount.get().setRole(userRequest.getRole());
            userAccount.get().setStatus(userRequest.getStatus());
            userAccountRepository.save(userAccount.get());
            return user;
        }
    }

    @Override
    public User getUserById(Integer userId) throws ResourceNotFoundException{
        return userRepository.findById(userId).orElseThrow(()-> new ResourceNotFoundException("User Not Found"));
    }

    @Override
    public List<User> getAllUser() {
      return userRepository.findAll();
    }

    @Override
    public MessageResponse deleteUser(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow(()-> new ResourceNotFoundException("User Not Found"));
        Integer userAccountId = userAccountRepository.FindByUserId(user.getId());
        Optional<UserAccount> userAccount = userAccountRepository.findById(userAccountId);

        if ( !userAccount.isPresent()){
            throw new ResourceNotFoundException("User Not Found. cant updated");
        }

        else{
            userAccount.get().setStatus("InActive");
            userAccountRepository.save(userAccount.get());
            return new MessageResponse("User Deleted Successfully");
        }

    }
    @Override
    public Integer getActiveCount(){
       return userRepository.countByActive();
    }

    @Override
    public Integer getPendingCount(){
        return userRepository.countByPending();
    }

    @Override
    public Integer getTotalCount(){
        return userRepository.countTotal();
    }

    @Override
    public Integer getDeletedCount(){
        return userRepository.countByInActive();
    }

    @Override
    public List<DTOProjection> getUsers(){
        return userRepository.GetUsers();
    }

    @Override
    public Optional<User> editUser(Integer userId, UserRequest userRequest ) throws ResourceNotFoundException{
        Optional<User> user = userRepository.findById((userId));

        if (!user.isPresent()){
            throw new ResourceNotFoundException("User Not Found. cant updated");
        }
        else{

            user.get().setFirstName(userRequest.getFirstName());
            user.get().setLastName(userRequest.getLastName());
            user.get().setUserName(userRequest.getUserName());
            user.get().setPhoneNumber(userRequest.getPhoneNumber());
            user.get().setEmail(userRequest.getEmail());
            user.get().setPassword(userRequest.getPassword());
            user.get().setDesignation(userRequest.getDesignation());
            user.get().setGender(userRequest.getGender());
            userRepository.save(user.get());
            return user;
        }
    }

    public UserDTO convertEntitytoDTO (User user){
        ModelMapper mapper = new ModelMapper();
        UserDTO userDTO = new UserDTO();
        userDTO = mapper.map(user, UserDTO.class);
        return userDTO;
    }

    public User convertDTOtoEntity (UserDTO userDTO){
        ModelMapper mapper = new ModelMapper();
        User user = new User();
        user = mapper.map(userDTO, User.class);
        return user;
    }

    @Override
    public DTOProjection getLoginUser(String email){
        return userRepository.GetLoginUser(email);
    }

}
