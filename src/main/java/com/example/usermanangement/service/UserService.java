package com.example.usermanangement.service;

import com.example.usermanangement.data.DTO.UserDTO;
import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.payloads.request.UserRequest;
import com.example.usermanangement.data.payloads.response.MessageResponse;

import com.example.usermanangement.data.repository.DTOProjection;
import com.example.usermanangement.exception.ResourceNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface UserService {
    MessageResponse createUser(UserRequest userRequest);
    MessageResponse createUserBySignUp(UserRequest userRequest);
    User updateUser(Integer userId, UserRequest userRequest ) throws ResourceNotFoundException;
    Optional<User> editUser(Integer userId, UserRequest userRequest );
    MessageResponse deleteUser(Integer userId);
    User getUserById(Integer userId) throws Exception;
    List<User> getAllUser();
    Integer getActiveCount();
    Integer getTotalCount();
    Integer getPendingCount();
    Integer getDeletedCount();
    List<DTOProjection> getUsers();
    DTOProjection getLoginUser(String email);
}
