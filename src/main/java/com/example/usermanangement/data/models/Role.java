package com.example.usermanangement.data.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;


@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private Roles role;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "role", fetch = FetchType.LAZY)
    @JsonManagedReference(value = "role_account")
    private Set<UserAccount> userAccount;



    //    constructor
    public Role() {}

//    Setter & getter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public Set<UserAccount> getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(Set<UserAccount> userAccount) {
        this.userAccount = userAccount;
    }


    @Override
    public String toString() {
        return "Role{" +
                "id = " + id +
                ", role = " + role +
                ", userAccount = " + userAccount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role rol = (Role) o;
        return  Objects.equals(id, rol.id)   && role == rol.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role);
    }
}
