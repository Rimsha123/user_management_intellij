package com.example.usermanangement.data.models;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Objects;


@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String firstName;
    private String lastName;
    private String userName;
    private String phoneNumber;
    private String email;
    private String password;
    private String gender;
    private String designation;

    @OneToOne(cascade = CascadeType.MERGE, mappedBy = "user", fetch = FetchType.LAZY)
    @JsonManagedReference(value = "user_account")
    private UserAccount userAccount;


    //    constructor
    public User() {}

//    getter & setter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public String toString(){
        return
                "User {" +
                "id = " + id +
                ", firstName = '" + firstName + '\'' +
                ", lastName = '" + lastName + '\'' +
                ", userName = '" + userName + '\'' +
                ", phoneNumber = '" + phoneNumber + '\'' +
                ", email = '" + email + '\'' +
                ", password = '" + password + '\'' +
                ", gender = '" + gender + '\'' +
                ", designation = '" + designation + '\'' +
                        ", userAccount = '" + userAccount + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o){
        if(this==o){
            return true;
        }

        if(o==null || getClass() != o.getClass()){
            return false;
        }

        User user = (User) o;
        return  Objects.equals(id, user.id) && Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) && Objects.equals(userName, user.userName) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(email, user.email) && Objects.equals(password, user.password) &&
                Objects.equals(gender, user.gender) && Objects.equals(designation, user.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, userName , phoneNumber, email, password, gender, designation);
    }
}
