package com.example.usermanangement.data.models;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class UserAccount extends Object {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JsonBackReference(value = "user_account")
    @JoinColumn(name = "userID", referencedColumnName = "id")
    private User user;


    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY, optional = false)
    @JsonBackReference(value = "role_account")
    @JoinColumn(name = "roleID", referencedColumnName = "id")
    private Role role;

    private String status;

//    constructor

    public UserAccount() {}

//    setter and Getter

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User Account { " +
                "id=" + id +
                ", userID = '" +  user+ '\'' +
                ", roleID = '" + role + '\'' +
                ", status = " + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount userAccount = (UserAccount) o;
        return Objects.equals(id, userAccount.id)   && Objects.equals(status, userAccount.status)
                && user == userAccount.user && role == userAccount.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, role, status);
    }

}
