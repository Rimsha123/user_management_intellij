package com.example.usermanangement.data.repository;

public interface DTOProjection {
    Integer getId();
    String getFirst_Name();
    String getEmail();
    String getUser_Name();
    String getRole();
    String getStatus();
    String getPhone_number();
    String getDesignation();
    String getLast_name();
    String getGender();
    String getPassword();


}
