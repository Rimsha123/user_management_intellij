package com.example.usermanangement.data.repository;

import com.example.usermanangement.data.DTO.UserDTO;
import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.models.UserAccount;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT user.*, role.role, user_account.status\n" +
            "FROM ((user_account\n" +
            "INNER JOIN user ON user.id = user_account.userid)\n" +
            "INNER JOIN role ON role.id = user_account.roleid)", nativeQuery = true)
    List<DTOProjection> GetUsers();

    @Query(value = "SELECT user.*, role.role, user_account.status\n" +
            "FROM ((user_account\n" +
            "INNER JOIN user ON user.id = user_account.userid)\n" +
            "INNER JOIN role ON role.id = user_account.roleid)\n" +
            "where user.email =:email", nativeQuery = true)
  DTOProjection GetLoginUser(String email);

    @Query(value = "Select count(*)  from user u, user_account ua  where ua.userid=u.id", nativeQuery = true)
    Integer countTotal();

     @Query(value = "Select count(*)  from user u, user_account ua  where ua.userid=u.id && ua.status = 'Active' ", nativeQuery = true)
     Integer countByActive();

    @Query(value = "Select count(*)  from user u, user_account ua  where ua.userid=u.id && ua.status = 'Pending' ", nativeQuery = true)
    Integer countByPending();

    @Query(value = "Select count(*)  from user u, user_account ua  where ua.userid=u.id && ua.status = 'InActive' ", nativeQuery = true)
    Integer countByInActive();
}