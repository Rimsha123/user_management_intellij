package com.example.usermanangement.data.repository;

import com.example.usermanangement.data.models.UserAccount;
import com.example.usermanangement.data.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Integer> {
    @Query(value = "SELECT user.*, role.role, user_account.status\n" +
            "FROM ((user_account\n" +
            "INNER JOIN user ON user.id = user_account.userid)\n" +
            "INNER JOIN role ON  user_account.roleid = role.id )", nativeQuery = true)
    List<UserAccount> GetUsers();

    @Query(value = "select user_account.id from user_account where user_account.userid =:user", nativeQuery = true)
    Integer FindByUserId(Integer user);
}