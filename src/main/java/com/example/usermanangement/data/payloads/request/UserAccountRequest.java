package com.example.usermanangement.data.payloads.request;

import com.example.usermanangement.data.models.User;
import com.example.usermanangement.data.models.Role;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserAccountRequest {
    @NotBlank
    @NotNull
    private User user;
    @NotBlank
    @NotNull
    private Role role;
    @NotBlank
    @NotNull
    private String status;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
