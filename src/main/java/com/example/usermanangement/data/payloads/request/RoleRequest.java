package com.example.usermanangement.data.payloads.request;

import com.example.usermanangement.data.models.Roles;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class RoleRequest {
    @Enumerated(EnumType.STRING)
    private Roles role;

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
